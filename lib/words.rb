class Words

  def dictionary
    dictionary = File.open("dictionary.txt", "r")
    dictionary.each_line do |line|
      puts line
    end
    dictionary.close
  end

  def find_sequences(dictionary)
    sequences = {}

    dictionary.each do |word|
      sequences << word.sequence
    end
  end

  def sequence(word)
    word.length < 4
    word.match(/\A[A-Za-z]+\z/)
  end

  def order(words)
    words.sort_by {|word| word.downcase}
  end

  def output_sequences(sequences)
    sequences.each do |key, value|
      sequences << key
      words << value
    end

    sequences_file = File.open( "sequences.txt", "w" )
    sequences_file << sequences.order

    words_file = File.open( "words.txt", "w" )
    words_file << words.order
  end
end
